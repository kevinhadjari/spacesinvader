package vue;

import modele.IMobile;
import modele.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class SpriteA extends ASprite {
	//protected Image image;
	int couleur;
	PImage sprite;
	public SpriteA(PApplet fenetre, IMobile mobile, int couleur) {
		super(fenetre, mobile);
		this.couleur = couleur;
		sprite = fenetre.loadImage("./images/ennemis/spaceinvader.png");
		sprite.resize(70,0);

	}

	@Override
	public void dessiner() {
		Position position = mobile.getPosition();
		int x = position.getXpix();
		int y = position.getYpix();
		/*
		fenetre.fill(couleur,0,0);
		fenetre.ellipse(x,y,40,40);
		*/
		fenetre.image(sprite,x,y);
	}

}
