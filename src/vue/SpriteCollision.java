package vue;

import modele.IMobile;
import modele.Position;
import processing.core.PApplet;
import processing.core.PImage;
import java.util.ArrayList;
import java.util.TimerTask;

public class SpriteCollision extends ASprite  {
    ArrayList<PImage> sprite = new ArrayList<>();


    public SpriteCollision(PApplet fenetre, IMobile mobile) {
        super(fenetre, mobile);

        for (int i = 1; i <= 9; i++) {

            sprite.add(fenetre.loadImage("./images/explosion/64x48/explosion1_000" + i+".png"));

        }

        for(int j =10;j<=90;j++){
            if (j>1)
                sprite.add(fenetre.loadImage("./images/explosion/64x48/explosion1_00" + j+".png"));

        }


    }    private static boolean run = true;


    @Override
    public void dessiner() {
        java.util.Timer timer;
        timer = new java.util.Timer();
        if(sprite.size() >0)
            timer.schedule(new MaTask2(), 0);
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        timer.cancel();
        timer.purge();

    }
    int i =0;
    private class MaTask2 extends TimerTask {

        @Override
        public void run() {
            if (i < sprite.size() -1) i++;
            else i = 0;
            Position position = mobile.getPosition();

            int x = position.getXpix();
            int y = position.getYpix();


            fenetre.image(sprite.get(i), x, y);

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

        }}

}
