package vue;

import modele.IMobile;
import modele.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class SpriteJoueur extends ASprite {
	//protected Image image;
	 PImage sprite;
	boolean vert = true;
	public SpriteJoueur(PApplet fenetre, IMobile mobile) {
		super(fenetre, mobile);
		sprite = fenetre.loadImage("./images/joueur/image00.png");

		sprite.resize(70,0);

	}

	@Override
	public void dessiner() {
		Position position = mobile.getPosition();
		int x = position.getXpix();
		int y = position.getYpix();
		/*
		fenetre.fill(couleur,0,0);
		fenetre.ellipse(x,y,40,40);
		*/
		for (int i = 0; i < 2; i++) {
			fenetre.image(sprite,x,y);

		}
	}

	public PImage getSprite() {
		return sprite;
	}
}
