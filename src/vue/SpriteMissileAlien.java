package vue;

import modele.IMobile;
import modele.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class SpriteMissileAlien extends ASprite {
    //protected Image image;
    PImage spriteA;
    PImage spriteB;


    public SpriteMissileAlien(PApplet fenetre, IMobile mobile) {
        super(fenetre, mobile);
        spriteA = fenetre.loadImage("./images/ennemis/greenLaserRay.png");
        spriteA.resize(20, 0);
//        spriteB = fenetre.loadImage("./images/ennemis/greenLaserRay.png");
   //     spriteB.resize(20, 0);
    }

    @Override
    public void dessiner() {
        Position position = mobile.getPosition();
        int x = position.getXpix()+25;
        int y = position.getYpix()+50;


            fenetre.image(spriteA, x, y);

         //   fenetre.image(spriteB, x, y);



    }
}
