package vue;

import modele.IMobile;
import modele.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class SpriteB extends ASprite {
	//protected Image image;
	int couleur;
	PImage sprite;

	public SpriteB(PApplet fenetre, IMobile mobile, int couleur) {
		super(fenetre, mobile);
		this.couleur = couleur;
		sprite = fenetre.loadImage("./images/ennemis/spaceinvader2.png");
		sprite.resize(90,0);
	}

	@Override
	public void dessiner() {
		Position position = mobile.getPosition();
		int x = position.getXpix();
		int y = position.getYpix();
		/*
		fenetre.fill(0,0,couleur);
		fenetre.rect(x-20,y-20,40,40);
		*/
		fenetre.image(sprite,x,y);

	}

}
