package vue;

import modele.IMobile;
import modele.Position;
import processing.core.PApplet;
import processing.core.PImage;

public class SpriteMissileJoueur extends ASprite {
    //protected Image image;
    PImage sprite;

    public SpriteMissileJoueur(PApplet fenetre, IMobile mobile) {
        super(fenetre, mobile);
        sprite = fenetre.loadImage("./images/joueur/tirJoueur.png");

        sprite.resize(20,0);

    }

    @Override
    public void dessiner() {
        Position position = mobile.getPosition();
        int x = position.getXpix();
        int y = position.getYpix();

        for (int i = 0; i < 2; i++) {
            fenetre.image(sprite,x,y);

        }
    }
}
