package controleur;

import modele.*;
import processing.core.PApplet;
import processing.core.PImage;
import vaisseauxGraphiques.AVaisseau;
import vaisseauxGraphiques.Vaisseau;
import vue.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class FenetreControleur extends PApplet {

    ArrayList<AVaisseau> vaisseaux = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> bouclier = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> missileJoueur = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> missileAlien = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> colission = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> toRemovevaisseaux = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> toRemovemissileJoueur = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> toRemoveBouclier = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau> toRemoveColission = new ArrayList<AVaisseau>();
    ArrayList<AVaisseau>  toRemovemissileAlien= new ArrayList<AVaisseau>();
    AVaisseau colissions;
    Vaisseau joueur;
    PImage fond;
    FenetreControleur thise;

    boolean gameover = false;
    int xJoueur;
    int yJoueur;
    int i =0;


    public void settings() {
        size(1024, 768);

    }

    public void setup() {
        // fenetre

        xJoueur = width / 2;
        yJoueur = 700;
        frameRate(60);
        fond = loadImage("./images/fond/universFond.jpg");
        // creation des vaisseaux
        boolean vert;
        for (int i = 0; i <= 15; i++) {
            vert = true;
            IMobile im = new MobileA(((double) 4 * i) / 10, new Position(400, 384));
            if ((i % 2) == 0)
                vaisseaux.add(new Vaisseau(im, new SpriteA(this, im, (i * 20) + 55)));
            else
                vaisseaux.add(new Vaisseau(im, new SpriteB(this, im, ((10 - i) * 20) + 55)));


        }
        for (int i = 0; i <= 10; i++) {
            IMobile im = new MobileB(((double) 3 * i) / 10, new Position(400, 384));
            if ((i % 2) == 0)
                vaisseaux.add(new Vaisseau(im, new SpriteA(this, im, (i * 20) + 55)));
            else
                vaisseaux.add(new Vaisseau(im, new SpriteB(this, im, ((10 - i) * 20) + 55)));
        }
        IMobile imJoueur = new MobileJoueur(this, new Position(width / 2, yJoueur));
        joueur = new Vaisseau(imJoueur, new SpriteJoueur(this, imJoueur));

        for (int i = 0; i <= 19; i++) {
            IMobile imBouclier;// = new MobileJoueur(((double) xJoueur), new Position(width / 2, yJoueur));
            int x = 1;
            int y;
            x += i * 50;

            if ((i % 2) == 0) {
                y = height - 250;
                imBouclier = new MobileBouclier(((double) x), new Position(x, y));
                bouclier.add(new Vaisseau(imBouclier, new SpriteBouclier(this, imBouclier)));
            } else {
                y = height - 200;
                imBouclier = new MobileBouclier(((double) x), new Position(x, y));
                bouclier.add(new Vaisseau(imBouclier, new SpriteBouclier(this, imBouclier)));
            }
        }
        missilesVaisseaux();

    }



    void missilesVaisseaux(){
    Timer timer;

    timer = new Timer();
    timer.schedule(new MaTasks(), 1000, 1000);
    thise = this;

}
//thread d'aleatoire
    private class MaTasks extends TimerTask {

        @Override
        public void run() {

           if(!gameover) {
               //pour une exception
                if (i < vaisseaux.size() - 1) i++;
                else  i = 0;
                //i=vaisseaux.size();
                int x = vaisseaux.get(i).getPosition().getXpix();
                int y = vaisseaux.get(i).getPosition().getYpix();

                IMobile imMissileAlien;// = new MobileJoueur(((double) xJoueur), new Position(width / 2, yJoueur));
                imMissileAlien = new MobileMissileAlien(thise, new Position(x, y));
                missileAlien.add(new Vaisseau(imMissileAlien, new SpriteMissileAlien(thise, imMissileAlien)));

                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

    }


    void colision() {


        for (AVaisseau v : vaisseaux) {

            for (AVaisseau mi : missileJoueur) {
                // if ((i % 2) == 0) {
                //y=height-250;
                int xm = mi.getPosition().getXpix() - 10;
                int xp = mi.getPosition().getXpix() + 10;
                int ym = mi.getPosition().getYpix() - 10;
                int yp = mi.getPosition().getYpix() + 10;


                if (v.getPosition().getXpix() > xm && v.getPosition().getXpix() < xp && v.getPosition().getYpix() > ym && v.getPosition().getYpix() < yp) {
                    toRemovevaisseaux.add(v);
                    toRemovemissileJoueur.add(mi);

                    MobileCollision imExplosion = new MobileCollision(this, new Position(v.getPosition().getXpix(), v.getPosition().getYpix()));
                    colission.add(new Vaisseau(imExplosion, new SpriteCollision(this, imExplosion)));
                }


            }

        }

        if (missileAlien.size() > 0){

                for (int i = missileAlien.size() - 1 ; i >= 0 ; i--) {
                 xm = missileAlien.get(i).getPosition().getXpix() - 10;
                 xp = missileAlien.get(i).getPosition().getXpix() + 10;
                 ym = missileAlien.get(i).getPosition().getYpix() - 10;
                 yp = missileAlien.get(i).getPosition().getYpix() + 10;

                    if(ym > this.width){
                     toRemovemissileAlien.add( missileAlien.get(i)) ;
                     }
                    for (AVaisseau b : bouclier) {
                    int xbm = b.getPosition().getXpix()+20;
                    int xbp = b.getPosition().getXpix()-20;
                    int ybm = b.getPosition().getYpix()+20;
                    int ybp = b.getPosition().getYpix()-20;
                    if (  ybm > ym && ybp < yp && xbm > xm && xbp < xp) {
                        MobileCollision imExplosion = new MobileCollision(this, new Position(b.getPosition().getXpix(), b.getPosition().getYpix()));
                        toRemoveBouclier.add(b);
                        toRemovemissileJoueur.add(missileAlien.get(i));
                        colission.add(new Vaisseau(imExplosion, new SpriteCollision(this, imExplosion)));



                    }
                }
                if (joueur.getPosition().getXpix() > xm && joueur.getPosition().getXpix() < xp && joueur.getPosition().getYpix() > ym && joueur.getPosition().getYpix() < yp) {
                    System.out.println("game over");
                    gameover = true;

                }
            }
    }

            for (int i = missileJoueur.size() - 1 ; i >= 0 ; i--) {
                xm = missileJoueur.get(i).getPosition().getXpix() - 10;
                xp = missileJoueur.get(i).getPosition().getXpix() + 10;
                ym = missileJoueur.get(i).getPosition().getYpix() - 10;
                yp = missileJoueur.get(i).getPosition().getYpix() + 10;

                if (missileJoueur.size() > 0){
                    if(ym < 0){
                        toRemovemissileJoueur.add( missileJoueur.get(i)) ;
                    }}
                for (AVaisseau b : bouclier) {
                    int xbm = b.getPosition().getXpix()+20;
                    int xbp = b.getPosition().getXpix()-20;
                    int ybm = b.getPosition().getYpix()+20;
                    int ybp = b.getPosition().getYpix()-20;
                    if (  ybm > ym && ybp < yp && xbm > xm && xbp < xp) {
                        MobileCollision imExplosion = new MobileCollision(this, new Position(b.getPosition().getXpix(), b.getPosition().getYpix()));
                        toRemoveBouclier.add(b);
                        toRemovemissileJoueur.add(missileAlien.get(i));

                        colission.add(new Vaisseau(imExplosion, new SpriteCollision(this, imExplosion)));
                        // col=true;



                    }
                }


        }
    }

    int xm,xp,ym,yp;



    public void draw() {


        if(gameover ==true) {
            this.image(this.loadImage("./images/gameover/gameOver.jpg"), 100, 100);
            vaisseaux.clear();
            bouclier.clear();
            colission.clear();
            missileAlien.clear();
            missileJoueur.clear();
            joueur =null;
        }
        else {
            // fond

            image(fond, 0, 0);
            // vaisseaux : deplacement et dessin

            joueur.dessiner();
            vaisseaux.removeAll(toRemovevaisseaux);
            missileJoueur.removeAll(toRemovemissileJoueur);
            bouclier.removeAll(toRemoveBouclier);
            colission.removeAll(toRemoveColission);
            missileAlien.removeAll(toRemovemissileAlien);
            for (AVaisseau v : vaisseaux) {
                v.deplacer();
                v.dessiner();

            }

            for (AVaisseau v : bouclier) {
                v.deplacer();
                v.dessiner();

            }
            for (AVaisseau v : missileJoueur) {
                v.dessiner();
                v.deplacer();

            }

           for (int i = missileAlien.size() - 1 ; i >= 0 ; i--) {
                missileAlien.get(i).dessiner();
                missileAlien.get(i).deplacer();


            }
            colision();


                for (int i = 0; i < colission.size(); i++) {

                   // colission.get(i).dessiner();
                    colissions = colission.get(i);

                }
                if (colission.size() > 0) {
                   colissions.dessiner();


        }
        }


    }

	public static void main(String... args) {
		PApplet.main(new String[] { "controleur.FenetreControleur" });
	}

	public void keyPressed() {
        if(gameover==false) {
            if (key == 'q' || keyCode == KeyEvent.VK_LEFT) {
                if (xJoueur > 10)
                    xJoueur -= 10;
                joueur.getPosition().setXpix(xJoueur);
            }
            // mo
            else if (key == 'd' || keyCode == KeyEvent.VK_RIGHT) {
                if (xJoueur < 950)
                    xJoueur += 10;
                joueur.getPosition().setXpix(xJoueur);
            } else if (key == 's' || keyCode == KeyEvent.VK_DOWN) {
                IMobile imMissileJoueur = new MobileMissileJoueur(this, new Position(xJoueur + 25, yJoueur - 50));
                missileJoueur.add(new Vaisseau(imMissileJoueur, new SpriteMissileJoueur(this, imMissileJoueur)));

            }
        }else{
            gameover=false;

            setup();
        }
	}



}
