package modele;

import processing.core.PApplet;

public class MobileBouclier implements IMobile {

    double t=0.0;
    double gap=-0.03;
    Position position;

    public MobileBouclier(double t, Position position){
        this.t = t;
        this.position = position;

    }
    @Override
    public void deplacer() {

    }

    @Override
    public Position getPosition() {
        return position;
    }


}
