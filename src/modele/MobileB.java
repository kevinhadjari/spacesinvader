package modele;

import processing.core.PApplet;

public class MobileB implements IMobile {
	double t=0.0;
	double gap=-0.03;
	int rayon=100;
	Position position;
	
	public MobileB(double t, Position position){
		this.t = t;
		this.position = position;
	}

	@Override
	public void deplacer() {
		t+=gap;
		int x = 480 + (int) (rayon*Math.sin(2*t))+(int) (rayon*Math.cos(t));
		int y = 200+(int) ((rayon*Math.sin(t)));
		position.setXpix(x);
		position.setYpix(y);
	}

	
	@Override
	public Position getPosition() {
		return position;
	}



}
