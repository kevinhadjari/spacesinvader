package modele;

import processing.core.PApplet;

public class MobileMissileAlien implements IMobile{
    // double t=0.0;
    int t=0;

    Position position;
    PApplet fenetre;
    public MobileMissileAlien(PApplet fenetres,Position position){
        this.fenetre = fenetre;
        this.position = position;

    }

    @Override
    public void deplacer() {
        t=position.getYpix();
        t+=3;
        position.setYpix(t);
    }


    @Override
    public Position getPosition() {
        return position;

    }
}
