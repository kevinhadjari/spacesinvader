package modele;

public class MobileA implements IMobile {
	double t=0.0;
	double gap=0.02;
	int rayon=270;
	Position position;
	
	public MobileA(double t, Position position){
		this.t = t;
		this.position = position;
	}

	@Override
	public void deplacer() {
		t+=gap;
		int x = 480+(int) (rayon*Math.cos(t));
		int y = 200+(int) (0.8*(rayon*Math.sin(t)));
		position.setXpix(x);
		position.setYpix(y);
	}

	
	@Override
	public Position getPosition() {
		return position;
	}

}
